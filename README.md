# Application web backend qui scanne les répertoires et les fichiers

## Contenu du dépôt : 

* Un fichier dockerfile qui permet de créer l'image /app.
* Un fichier .gitlab-ci.yml qui permet de déclencher des actions CICD : 
    * compile l'application (permet d'obtenir un fichier jar)
    * lance les tests
    * déploie l'application dans AWS Elastic Beanstalk (en copiant le fichier jar dans un bucket S3)

# Comment utiliser l'application : 

## Étapes préliminaires **obligatoires**

### 1. Créer une bdd RDS sur aws et donner les droits à tous (modifier les règles entrantes du groupe de sécurité de la bdd pour ajouter une règle qui autorise les connexions depuis toutes les adresses IP)

Changer 

* spring.datasource.url
* spring.datasource.username
* spring.datasource.password 

avec les informations de la bdd créée sur AWS dans le fichier **src/main/resources/application.properties** comme vu en cours.

### 2. Créer une application Elastic Beanstalk et un Bucket S3 qui contiendra le .jar

Pour n'avoir aucun problème par la suite, il faut appeler l'application **gl** dans les paramètres de Elastic Beanstalk, et **bucket-jar-proj** pour le Bucket S3. Il faut suivre les démarches que nous avons vu en cours.

### 3. Copier-Coller vos credentials dans ~/.aws/credentials

### 4. Variables secrètes à remplir sur gitlab pour la CI/CD (nécessaire seulement pour la CI/CD):

* AWS_ACCESS_KEY_ID
* AWS_SECRET_ACCESS_KEY
* AWS_DEFAULT_REGION (us-east-1 suffit)
* AWS_SECRET_TOKEN

## Utilisation en local : 

#### option 1 : via Maven

```
mvn package
mvn spring-boot:run
```

#### option 2 : via Docker

```
docker build -t nom_image:tag .
docker run -p 5000:5000 -v path/to/dir:/vol nom_image:tag
```

Attention, pour docker, on a besoin de faire un mapping entre le fichiers sur l'hôte et le container. Si vous souhaiter mapper par exemple tout le contenu de /Users/antoinedepaepe/Desktop/Tp_sys_distrib dans le container, vous ferez attention au fait que tout le contenu de Tp_sys_distrib sera dans /vol. Ainsi, les requêtes locales seront de la forme /vol/path/to/mapped/subdir.

### Endpoint exposé par l'application : 

* http://0.0.0.0:5000/swagger
* http://0.0.0.0:5000/create_scan/local_file_system?path=/Users/antoinedepaepe/Desktop/FTp_sys_distrib&filenameFilter=q6&extensionFilter=java&maxFiles=10&maxDepth=10
* http://0.0.0.0:5000/create_scan/S3_file_system?bucketName=bucketprojectapolline2&extensionFilter=txt&maxFiles=10&maxDepth=10
* http://0.0.0.0:5000/get_scan?id=5
* http://0.0.0.0:5000/get_all_scans
* http://0.0.0.0:5000/delete_scan?id=5
* http://0.0.0.0:5000/replay_scan?id=5
* http://0.0.0.0:5000/compare_scans?id1=5&id2=6

Consulter le swagger pour avoir tous les détails.

## Utilisation sur aws : 

Cliquer sur l'url de l'environnement Elastic Beanstalk

### Endpoint exposé par l'application : 

* /swagger
* /create_scan/S3_file_system?bucketName=bucketprojectapolline2&extensionFilter=txt&maxFiles=10&maxDepth=10
* /get_scan?id=6
* /get_all_scans
* /delete_scan?id=6
* /replay_scan?id=7
* /compare_scans?id1=1&id2=2

### 5. Déclencher les pipelines gitlab

```
git add .
git commit -m "nouveau commit"
git push
```
