package fr.ensai.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.ensai.demo.model.filesystem.FolderComponent;
import fr.ensai.demo.model.scan.Scan;
import fr.ensai.demo.model.strategy.LocalFileSystemScanner;
import fr.ensai.demo.model.strategy.S3FileSystemScanner;
import fr.ensai.demo.service.ScanService;

import java.util.Optional;

@RestController
public class ScanController {
    @Autowired
    private ScanService scanService;

    @GetMapping("/")
    public ResponseEntity<String> index() {
        return new ResponseEntity<>("Application de scan de fichiers", HttpStatus.OK);
    }

    @GetMapping("/create_scan/local_file_system")
    public ResponseEntity<Scan> getLocalFileSystemScan(@RequestParam String path,
                                        @RequestParam(required = false) String filenameFilter,
                                        @RequestParam(required = false) String extensionFilter,
                                        @RequestParam(required = false, defaultValue = "10") int maxFiles,
                                        @RequestParam(required = false, defaultValue = "10") int maxDepth) {
        // Creating the root directory
        
        Scan scan = scanService.createScan("local", path, filenameFilter, extensionFilter, maxFiles, maxDepth);
        scanService.saveScan(scan);                
        return new ResponseEntity<>(scan, HttpStatus.OK);

    }

    @GetMapping("/create_scan/S3_file_system")
    public ResponseEntity<Scan> getS3FileSystemScan(@RequestParam String bucketName,
                                @RequestParam(required = false) String filenameFilter,
                                @RequestParam(required = false) String extensionFilter,
                                @RequestParam(required = false, defaultValue = "10") int maxFiles,
                                @RequestParam(required = false, defaultValue = "10") int maxDepth){
        // String bucketName = "bucketprojectapolline";
       
        Scan scan = scanService.createScan("s3", bucketName, filenameFilter, extensionFilter, maxFiles, maxDepth);
        scanService.saveScan(scan);
        return new ResponseEntity<>(scan, HttpStatus.OK);

    }

    @GetMapping("/get_all_scans")
        public ResponseEntity<Iterable<Scan>> getAllScans() {
            Iterable<Scan> scans = scanService.getScans();
    
            return new ResponseEntity<>(scans, HttpStatus.OK);
        }
    

    @GetMapping("/get_scan")
        public ResponseEntity<Optional<Scan>> getScanById(@RequestParam Long id) {
            Optional<Scan> scan = scanService.getScanById(id);
            return new ResponseEntity<>(scan, HttpStatus.OK);
        }

    @DeleteMapping("/delete_scan")
    public ResponseEntity<String> deleteScan(@RequestParam Long id) {
        boolean deleted = scanService.deleteScan(id);
        if (deleted) {
            return new ResponseEntity<>("Scan with ID " + id + " deleted successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Scan with ID " + id + " not found", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/replay_scan")
    public ResponseEntity<String> updateScan(@RequestParam Long id) {
        Optional<Scan> scan = scanService.getScanById(id);
        if (scan.isPresent()) {  
            scanService.replayScan(id, scan.get());
            String body1 = "Scan " + id + " rejoué.";
            return new ResponseEntity<>(body1, HttpStatus.OK);
        }
        String body = "Scan " + id + " n'existe pas dans la bdd.";
        return new ResponseEntity<>(body, HttpStatus.OK); 
    }

    @GetMapping("/compare_scans")
    public ResponseEntity<String> compareScans(@RequestParam Long id1, 
                                               @RequestParam Long id2) {
        String differences = scanService.compareScans(id1,id2);
        // Générer une réponse HTML
        String htmlResponse = "<html><body><h1>Différences détectées :</h1><pre>" + differences + "</pre></body></html>";
        return new ResponseEntity<>(htmlResponse, HttpStatus.OK);
    }

}



